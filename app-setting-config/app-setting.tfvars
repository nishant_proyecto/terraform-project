name                = "app-setting"
cluster_name        = "boltehr"
environment         = "dev"
aws-region          = "us-east-2"
vpc_id              = ""
# availability_zones  = ["us-east-2c", "us-east-2b","us-east-2a"]
# private_subnets     = ["10.0.0.0/20", "10.0.32.0/20"]
# public_subnets      = ["10.0.16.0/20", "10.0.48.0/20"]
tsl_certificate_arn     = "arn:aws:acm:us-east-2:169913037044:certificate/158a6dac-f866-4336-99e5-6f9b68128748"
container_image         = "169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/appsetting-api:latest"
container_port          = 3010
container_environment   = [
        {
          "name": "DEBUG",
          "value": "APP"
        }
      ]
container_memory        = 512
