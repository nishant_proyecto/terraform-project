environment         = "dev"
aws-region          = "us-east-2"
cidr                = "172.32.0.0/16"
public_subnets      = ["172.32.48.0/20", "172.32.64.0/20", "172.32.80.0/20"]