module "vpc-config" {
  source         = "../modules/vpc"
  aws-region     = var.aws-region
  aws-access-key = var.aws-access-key
  aws-secret-key = var.aws-secret-key
  environment    = var.environment
  cidr           = var.cidr
  public_subnets = var.public_subnets
}
