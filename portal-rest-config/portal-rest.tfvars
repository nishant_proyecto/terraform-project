name                = "portal-rest"
cluster_name        = "boltehr"
environment         = "dev"
aws-region          = "us-east-2"
vpc_id              = ""
# availability_zones  = ["us-east-2c", "us-east-2b","us-east-2a"]
# private_subnets     = ["10.0.0.0/20", "10.0.32.0/20"]
# public_subnets      = ["10.0.16.0/20", "10.0.48.0/20"]
tsl_certificate_arn     = "arn:aws:acm:us-east-2:169913037044:certificate/158a6dac-f866-4336-99e5-6f9b68128748"
container_image         = "169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/cust-port-rest:latest"
container_port          = 3020
container_environment   = [
        {
          "name": "CLIENT_ID",
          "value": "0oa1k2ry53wcnrl4C357"
        },
        {
          "name": "DEBUG",
          "value": "fhir-kit-client:*"
        },
        {
          "name": "FHIR_SERVER",
          "value": "http://3.227.131.186:8181/baseR4/"
        },
        {
          "name": "IDHUB_API_APPID",
          "value": ""
        },
        {
          "name": "IDHUB_API_URL",
          "value": ""
        },
        {
          "name": "MONGO_URL",
          "value": "mongodb://ec2-3-16-237-154.us-east-2.compute.amazonaws.com:27017/emrsim"
        },
        {
          "name": "NOAUTH",
          "value": "true"
        },
        {
          "name": "NODE_TLS_REJECT_UNAUTHORIZED",
          "value": "0"
        },
        {
          "name": "NOSSL",
          "value": "true"
        },
        {
          "name": "OKTA_API_KEY",
          "value": "003TJaprcZMPmycS7s-FcpzO2cENEGUj_JttMEovIb"
        },
        {
          "name": "OKTA_API_URL",
          "value": "https://dev-327342.okta.com"
        },
        {
          "name": "PORT",
          "value": "3020"
        }
      ]
container_memory        = 512
