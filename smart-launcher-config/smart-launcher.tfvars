name                = "smart-launcher"
cluster_name        = "boltehr"
environment         = "dev"
aws-region          = "us-east-2"
vpc_id              = ""
# availability_zones  = ["us-east-2c", "us-east-2b","us-east-2a"]
# private_subnets     = ["10.0.0.0/20", "10.0.32.0/20"]
# public_subnets      = ["10.0.16.0/20", "10.0.48.0/20"]
tsl_certificate_arn     = "arn:aws:acm:us-east-2:169913037044:certificate/158a6dac-f866-4336-99e5-6f9b68128748"
container_image         = "169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/smart-launcher:latest"
container_port          = 8001
container_environment   = [
        {
          "name": "BASE_URL",
          "value": "https://smart-launcher-1.emrsimulator.com/"
        },
        {
          "name": "FHIR_SERVER_R2",
          "value": "https://r2.smarthealthit.org"
        },
        {
          "name": "FHIR_SERVER_R3",
          "value": "https://r3.smarthealthit.org"
        },
        {
          "name": "FHIR_SERVER_R4",
          "value": "https://boltehr.demo.smilecdr.com/fhir-request"
        },
        {
          "name": "LAUNCHER_PORT",
          "value": "8001"
        },
        {
          "name": "NODE_ENV",
          "value": "production"
        },
        {
          "name": "PICKER_CONFIG_R2",
          "value": "r2"
        },
        {
          "name": "PICKER_CONFIG_R3",
          "value": "r3"
        },
        {
          "name": "PICKER_CONFIG_R4",
          "value": "r4"
        },
        {
          "name": "PICKER_ORIGIN",
          "value": "https://patient-browser.smarthealthit.org"
        }
      ]
container_memory        = 512
