provider "aws" {
  access_key = var.aws-access-key
  secret_key = var.aws-secret-key
  region     = var.aws-region
  version    = "~> 2.0"
}

locals{
  az_names = data.aws_availability_zones.available.names
}

resource "aws_vpc" "this" {
  cidr_block       = var.cidr
  instance_tenancy = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name       = "${var.environment}-vpc"
  }
}
# Create subnet
resource "aws_subnet" "this" {
  count      = length(local.az_names)
  vpc_id     = aws_vpc.this.id
  cidr_block = var.public_subnets[count.index]
  availability_zone = local.az_names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "subnet-${var.environment}-${count.index + 1}"
  }
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id

  tags = {
    Name        = "${var.environment}-igw"
    Environment = var.environment
  }
}

data "aws_route_table" "this" {
  vpc_id = aws_vpc.this.id
}

resource "aws_route" "this" {
  route_table_id         = data.aws_route_table.this.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this.id
}

resource "aws_route_table_association" "this" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.this.*.id, count.index)
  route_table_id = data.aws_route_table.this.id
}