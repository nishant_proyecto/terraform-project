variable "cidr" {
  description = "Choose vpc cidr block"
  type        = string
}
variable "public_subnets" {
  type    = list(string)
}

variable "environment" {
  description = "the name of your environment, e.g. \"prod\""
}

variable "aws-region" {
  type        = string
  description = "AWS region to launch servers."
}

variable "aws-access-key" {
  type = string
}

variable "aws-secret-key" {
  type = string
}