provider "aws" {
  access_key = var.aws-access-key
  secret_key = var.aws-secret-key
  region     = var.aws-region
  version    = "~> 2.0"
}

module "security_groups" {
  source         = "./security-groups"
  name           = var.name
  vpc_id         = data.aws_vpc.this.id
  environment    = var.environment
  container_port = var.container_port
}

module "alb" {
  source = "./alb"
  name   = var.name
  vpc_id = data.aws_vpc.this.id
  # subnets             = module.vpc.public_subnets
  subnets             = data.aws_subnet_ids.this.ids
  environment         = var.environment
  alb_security_groups = [module.security_groups.alb]
  alb_tls_cert_arn    = var.tsl_certificate_arn
  health_check_path   = var.health_check_path
}

module "ecs" {
  source       = "./ecs"
  name         = var.name
  cluster_name = var.cluster_name
  environment  = var.environment
  region       = var.aws-region
  # subnets                     = module.vpc.private_subnets
  subnets                     = data.aws_subnet_ids.this.ids
  aws_alb_target_group_arn    = module.alb.aws_alb_target_group_arn
  execution_role_arn          = data.aws_iam_role.ecs_task_execution_role.arn
  ecs_service_security_groups = [module.security_groups.ecs_tasks]
  container_port              = var.container_port
  container_cpu               = var.container_cpu
  container_memory            = var.container_memory
  service_desired_count       = var.service_desired_count
  container_image             = var.container_image
  container_environment       = var.container_environment
  # container_secrets      = module.secrets.secrets_map
  # aws_ecr_repository_url = module.ecr.aws_ecr_repository_url
  # container_secrets_arns = module.secrets.application_secrets_arn
}
