# module "terraform_definition" {
#   source = "git::https://github.com/cloudposse/terraform-aws-ecs-container-definition.git?ref=tags/0.23.0"

#   container_name  = "${var.application_name}-container"
#   container_image = "169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/emrsim:latest"
#   environment = [
#     {
#       "name" : "MONGO_URL",
#       "value" : "mongodb://ec2-3-16-237-154.us-east-2.compute.amazonaws.com:27017/emrsim"
#     },
#     {
#       "name" : "ROOT_URL",
#       "value" : "https://emr.emrsimulator.com/"
#     }
#   ]
#   port_mappings = [
#         {
#           "hostPort": "${var.container_port}",
#           "protocol": "tcp",
#           "containerPort": "${var.container_port}"
#         }
#       ]
#   log_configuration = {
#         "logDriver": "awslogs",
#         "secretOptions": null,
#         "options": {
#           "awslogs-group": "/ecs/first-run-task-definition",
#           "awslogs-region": "us-east-2",
#           "awslogs-stream-prefix": "ecs"
#         }
#       }
# }

# resource "aws_ecs_task_definition" "this" {
#   container_definitions    = module.terraform_definition.json
#   # container_definitions    = <<TASK_DEFINITION
#   # [ {
#   #   "name":"terraform-container",
#   #   "image":"169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/emrsim:latest",
#   #   "portMappings": [
#   #           {
#   #             "hostPort": 3000,
#   #             "protocol": "tcp",
#   #             "containerPort": 3000
#   #           }
#   #         ],
#   #   "environment": [
#   #       {
#   #         "name": "MONGO_URL",
#   #         "value": "mongodb://ec2-3-16-237-154.us-east-2.compute.amazonaws.com:27017/emrsim"
#   #       },
#   #       {
#   #         "name": "ROOT_URL",
#   #         "value": "https://emr.emrsimulator.com/"
#   #       }
#   #     ],
#   #     "logConfiguration" : {
#   #       "logDriver": "awslogs",
#   #       "secretOptions": null,
#   #       "options": {
#   #         "awslogs-group": "/ecs/first-run-task-definition",
#   #         "awslogs-region": "us-east-2",
#   #         "awslogs-stream-prefix": "ecs"
#   #       }
#   #     } }
#   # ]
#   # TASK_DEFINITION
#   family                   = local.task_definition_name
#   requires_compatibilities = [local.launch_type]
#   # task_role_arn            = data.aws_iam_role.devadmin.arn
#   execution_role_arn = data.aws_iam_role.ecs_task_execution_role.arn

#   cpu          = "256"
#   memory       = "512"
#   network_mode = "awsvpc"
# }
