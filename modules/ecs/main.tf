locals {
  launch_type      = "FARGATE"
}

resource "aws_ecs_cluster" "this" {
  name = "${var.cluster_name}-cluster-${var.environment}"
  tags = {
    Name        = "${var.cluster_name}-cluster-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_ecs_service" "this" {
  name        = "${var.name}-service-${var.environment}"
  cluster     = aws_ecs_cluster.this.arn
  launch_type = local.launch_type

  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 0
  desired_count                      = 1

  task_definition                    = aws_ecs_task_definition.this.arn

  load_balancer {
    target_group_arn = var.aws_alb_target_group_arn
    container_name   = "${var.name}-container-${var.environment}"
    container_port   = var.container_port
  }

  network_configuration {
    assign_public_ip = true
    security_groups  = var.ecs_service_security_groups 
    subnets          = var.subnets
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.name}-task-${var.environment}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.container_cpu
  memory                   = var.container_memory
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.execution_role_arn
  # execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  # task_role_arn            = aws_iam_role.ecs_task_role.arn
  container_definitions = jsonencode([{
    name        = "${var.name}-container-${var.environment}"
    image       = "${var.container_image}"
    essential   = true
    environment = var.container_environment
    portMappings = [{
      protocol      = "tcp"
      containerPort = var.container_port
      hostPort      = var.container_port
    }]
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        awslogs-group         = aws_cloudwatch_log_group.this.name
        awslogs-stream-prefix = "ecs"
        awslogs-region        = var.region
      }
    }
    # secrets = var.container_secrets
  }])

  tags = {
    Name        = "${var.name}-task-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_cloudwatch_log_group" "this" {
  name = "/ecs/${var.name}-task-${var.environment}"

  tags = {
    Name        = "${var.name}-task-${var.environment}"
    Environment = var.environment
  }
}