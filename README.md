Installation Tool -
- https://www.terraform.io/downloads.html

How to use -
* As per deployment environment 

- EMR-SIM Environment (make changes in main.tf as per development)
    - cd emrsim-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=emrsim.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=emrsim.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=emrsim.tfvars

- App Setting Environment (make changes in main.tf as per development)
    - cd app-setting-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=app-setting.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=app-setting.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=app-setting.tfvars
    
- Portal Rest API Environment (make changes in main.tf as per development)
    - cd portal-rest-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=portal-rest.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=portal-rest.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=portal-rest.tfvars

- Smart Launcher API Environment (make changes in main.tf as per development)
    - cd smart-launcher-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=smart-launcher.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=smart-launcher.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=smart-launcher.tfvars

- Portal UI API Environment (make changes in main.tf as per development)
    - cd portal-ui-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=portal-ui.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=portal-ui.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=portal-ui.tfvars

- VPC creation
    - cd vpc-config/
    - terraform init
    - terraform plan -var-file=secrets.tfvars -var-file=vpc.tfvars
    - terraform apply -var-file=secrets.tfvars -var-file=vpc.tfvars
    - terraform destroy -var-file=secrets.tfvars -var-file=vpc.tfvars