module "dev" {
  source         = "../modules"
  aws-region     = var.aws-region
  aws-access-key = var.aws-access-key
  aws-secret-key = var.aws-secret-key
  name           = var.name
  cluster_name   = var.cluster_name
  environment    = var.environment
  # availability_zones    = var.availability_zones
  container_image       = var.container_image
  container_port        = var.container_port
  container_environment = var.container_environment
  container_memory      = var.container_memory
  tsl_certificate_arn   = var.tsl_certificate_arn
  vpc_id                = var.vpc_id
}
