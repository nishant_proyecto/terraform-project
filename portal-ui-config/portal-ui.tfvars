name                = "portal-ui"
cluster_name        = "boltehr"
environment         = "dev"
aws-region          = "us-east-2"
vpc_id              = ""
# availability_zones  = ["us-east-2c", "us-east-2b","us-east-2a"]
# private_subnets     = ["10.0.0.0/20", "10.0.32.0/20"]
# public_subnets      = ["10.0.16.0/20", "10.0.48.0/20"]
tsl_certificate_arn     = "arn:aws:acm:us-east-2:169913037044:certificate/158a6dac-f866-4336-99e5-6f9b68128748"
container_image         = "169913037044.dkr.ecr.us-east-2.amazonaws.com/boltehr2019/ui-customer-portal:latest"
container_port          = 3030
container_environment   = [
        {
          "name": "emrSimUrl",
          "value": "https://emr.emrsimulator.com/"
        },
        {
          "name": "ENV",
          "value": "DEV"
        },
        {
          "name": "fhirServerApiUrl",
          "value": "https://portal-1-rest-api.emrsimulator.com/fhir"
        },
        {
          "name": "oauthClientId",
          "value": "0oa1k2ry53wcnrl4C357"
        },
        {
          "name": "oauthIssuer",
          "value": "https://dev-327342.okta.com/oauth2/default"
        },
        {
          "name": "PORT",
          "value": "3030"
        },
        {
          "name": "portalApiUrl",
          "value": "https://portal-1-rest-api.emrsimulator.com/"
        },
        {
          "name": "settingsApiUrl",
          "value": "https://app-settings-1-rest-api.emrsimulator.com/"
        }
      ]
container_memory        = 512
